<?php

declare(strict_types=1);

namespace ImageServer\Action;

use ImageServer\ActionInterface;
use ImageServer\Client;
use ImageServer\Model\Image;
use ImageServer\Service\ImageResize;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ImageDisplay
 * @package ImageServer\Action
 */
class ImageDisplay implements ActionInterface
{
    /**
     * @var Request
     */
    private Request $request;

    /**
     * @var string
     */
    private string $imagesStoragePath;

    /**
     * @var array
     */
    private array $imageMapping;

    /**
     * Route constructor.
     * @param Request $request
     * @param string $imagesStoragePath
     */
    public function __construct(Request $request, string $imagesStoragePath)
    {
        $this->request = $request;
        $this->imagesStoragePath = $imagesStoragePath;
        $this->imageMapping = \Spyc::YAMLLoad(getenv('IMAGES_MAPPING_FILE'));
    }

    /**
     * @param Client $client
     * @throws \Exception
     */
    public function handle(Client $client): void
    {
        $fileName = $this->imageMapping[$this->request->query->get('pid')] ?? '';

        $imagePath = $this->getImagePath($fileName);

        if (!is_readable($imagePath)) {
            throw new \Exception('Image not found', 404);
        }

        $this->setHeaders($imagePath);

        if (getenv('IMAGES_FORCE_SIZE_ENABLED') === 'true') {
            list($width, $height) = getimagesize($imagePath);

            $sourceImage = (new Image())->setWidth($width)->setHeight($height);
            $referralImage = (new Image())
                ->setWidth((int) getenv('IMAGES_FORCE_SIZE_WIDTH'))
                ->setHeight((int) getenv('IMAGES_FORCE_SIZE_HEIGHT'));
            $outputImageSize = ImageResize::calculateSize($sourceImage, $referralImage);
            $outputImage = ImageResize::resizeImage($imagePath, $outputImageSize);

            if (getenv('IMAGES_WATERMARK_ENABLED') === 'true') {
                $watermarkPath = getenv('IMAGES_WATERMARK_FILE');
                $outputImage = ImageResize::addWatermarkImage($watermarkPath, $outputImage);
            }

            imagepng($outputImage);
        } else {
            $imageFile = fopen($imagePath, 'rb');

            fpassthru($imageFile);
        }
    }

    /**
     * @param string $imageFile
     * @return string
     */
    private function getImagePath(string $imageFile): string
    {
        return $this->imagesStoragePath . $imageFile;
    }

    /**
     * @param string $photoPath
     */
    private function setHeaders(string $photoPath): void
    {
        header("Content-Type: image/png");
        header("Content-Length: " . filesize($photoPath));
    }
}
