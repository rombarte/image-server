<?php

declare(strict_types=1);

namespace ImageServer;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class Server
 * @package ImageServer
 */
class Server
{
    /**
     * @var string
     */
    private string $imagesStoragePath;

    /**
     * Server constructor.
     * @param string $imagesStoragePath
     */
    public function __construct(string $imagesStoragePath)
    {
        $this->imagesStoragePath = $imagesStoragePath;
    }

    /**
     * @throws \Exception
     */
    public function handleRequest(): void
    {
        $request = Request::createFromGlobals();
        $route = ActionFactory::createFromRequest($request, $this->imagesStoragePath);
        $client = Client::createFromRequest($request);
        $route->handle($client);
    }
}
