<?php

declare(strict_types=1);

namespace ImageServer;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class Client
 * @package ImageServer
 */
class Client
{
    /**
     * @var int
     */
    private int $deviceWidth;

    /**
     * @var int
     */
    private int $deviceHeight;

    /**
     * @var bool
     */
    private bool $isMobile;

    /**
     * Client constructor.
     * @param int $deviceWidth
     * @param int $deviceHeight
     * @param bool $isMobile
     */
    public function __construct(int $deviceWidth, int $deviceHeight, bool $isMobile)
    {
        $this->deviceWidth = $deviceWidth;
        $this->deviceHeight = $deviceHeight;
        $this->isMobile = $isMobile;
    }

    /**
     * @param $request
     * @return Client
     */
    public static function createFromRequest(Request $request): self
    {
        return new self(
            (int) $request->get('deviceWidth'),
            (int) $request->get('deviceHeight'),
            (bool) $request->get('isMobile'),
        );
    }
}
