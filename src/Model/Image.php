<?php

declare(strict_types=1);

namespace ImageServer\Model;

/**
 * Class Image
 * @package ImageServer\Model
 */
class Image
{
    /**
     * @var int
     */
    private int $width = 0;

    /**
     * @var int
     */
    private int $height = 0;

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return self
     */
    public function setWidth(int $width): self
    {
        if ($width < 0) {
            throw new \InvalidArgumentException('Width must equals or be more than 0');
        }
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return self
     */
    public function setHeight(int $height): self
    {
        if ($height < 0) {
            throw new \InvalidArgumentException('Height must equals or be more than 0');
        }
        $this->height = $height;
        return $this;
    }
}
