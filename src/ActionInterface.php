<?php

declare(strict_types=1);

namespace ImageServer;

interface ActionInterface
{
    /**
     * @param Client $client
     */
    public function handle(Client $client): void;
}
