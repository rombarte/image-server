<?php

declare(strict_types=1);

namespace ImageServer\Service;

use ImageServer\Model\Image;

/**
 * Class ImageResize
 * @package ImageServer\Service
 */
class ImageResize
{
    public const ALLOWED_IMAGE_TYPES = [
        IMAGETYPE_PNG,
    ];

    /**
     * @param string $photoPath
     * @param Image $image
     * @return resource
     * @throws \Exception
     */
    public static function resizeImage(string $photoPath, Image $image)
    {
        list($width, $height, $type) = getimagesize($photoPath);

        if (!in_array($type, self::ALLOWED_IMAGE_TYPES)) {
            throw new \Exception('Allowed image types are: ' . implode(', ', self::ALLOWED_IMAGE_TYPES));
        }

        $output = imagecreatetruecolor($image->getWidth(), $image->getHeight());
        $source = imagecreatefrompng($photoPath);

        imagecopyresized($output, $source, 0, 0, 0, 0, $image->getWidth(), $image->getHeight(), $width, $height);

        return $output;
    }

    /**
     * @param Image $sourceImage
     * @param Image $referralImage
     * @return Image
     */
    public static function calculateSize(Image $sourceImage, Image $referralImage): Image
    {
        $widthRatio = $referralImage->getWidth() / $sourceImage->getWidth();
        $heightRatio = $referralImage->getHeight() / $sourceImage->getHeight();

        $ratio = ($widthRatio < 1 || $heightRatio < 1) ? max($widthRatio, $heightRatio) : min($widthRatio, $heightRatio);

        $newWidth = (int) round($ratio * $sourceImage->getWidth());
        $newHeight = (int) round($ratio * $sourceImage->getHeight());

        return $sourceImage->setWidth($newWidth)->setHeight($newHeight);
    }

    /**
     * @param string $watermarkPath
     * @param $outputImage
     * @return mixed
     */
    public static function addWatermarkImage(string $watermarkPath, $outputImage)
    {
        $watermark = imagecreatefrompng($watermarkPath);

        list($watermarkWidth, $watermarkHeight) = getimagesize($watermarkPath);
        list($imageWidth, $imageHeight) = [imagesx($outputImage), imagesy($outputImage)];

        imagecopy(
            $outputImage,
            $watermark,
            $imageWidth - $watermarkWidth,
            $imageHeight - $watermarkHeight,
            0,
            0,
            $watermarkWidth,
            $watermarkHeight
        );

        return $outputImage;
    }
}
