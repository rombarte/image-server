<?php

declare(strict_types=1);

namespace ImageServer;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class ActionFactory
 * @package ImageServer
 */
class ActionFactory
{
    /**
     * @param Request $request
     * @param string $imagesStoragePath
     * @return ActionInterface
     * @throws \Exception
     */
    public static function createFromRequest(Request $request, string $imagesStoragePath): ActionInterface
    {
        if ($request->query->has('pid')) {
            return new Action\ImageDisplay($request, $imagesStoragePath);
        }
        throw new \Exception('Route not found');
    }
}
