const fs = require('fs');

module.exports = (on, config) => {
    on('task', {
        renameFile({source, destination}) {
            fs.renameSync(source, destination);
            return true;
        }
    })
};
