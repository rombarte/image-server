# Image server
Application for hosting images on websites

## Features
1. Host only images which you wants (by mapping configuration)
2. Resize images if have incorrect dimensions
3. Add custom watermark at image corner

## Technology stack
* Apache
* Docker
* GitLab CI
* PHP 7.4

## Usage
1. Build services:

    ```sh
    docker-compose build
    docker-compose up --no-start
    ```
2. Start services:

    ```sh
    docker-compose start
    ```
3. Copy images to `.docker/nfs/images` directory.
4. Create image mapping file, f.e:

    ```yaml
    b6x0z: wall.png
    ```
4. Launch application and go to:
    * `http://localhost/index.php?pid=b6x0z` (shows image `wall.png` from `.docker/nfs/images` directory)

5. Run integration tests:

    First time is necessary to install dependencies:

    ```sh 
    docker exec -it -w /var/www/html cypress12.13 npm install
    ```
    
    Then you can run tests:
    
    ```sh 
    docker exec -it -w /var/www/html cypress12.13 npm run makeIntegrationTests
    ```
6. Run code sniffer checks:

    ```sh
    docker exec -it php7.4 composer install
    docker exec -it php7.4 composer checkCodeSecurity
    docker exec -it php7.4 composer checkCodeStyle
    ```
## TO DO
1. Add example files for stable cypress tests.
2. Add more cypress tests.
3. Add pipeline for cypress tests.

## Licence
All files in this repository are available under the MIT license. You can find more information and a link to the license content on the [Wikipedia](https://en.wikipedia.org/wiki/MIT_License) page.