<?php

declare(strict_types=1);

require_once "../vendor/autoload.php";

try {
    $dotEnv = Dotenv\Dotenv::create(dirname(__DIR__));
    $dotEnv->load();

    $logger = new Monolog\Logger('image-server');
    $logger->pushHandler(new Monolog\Handler\StreamHandler(getenv('ERROR_LOG_FILE')));

    set_error_handler(
        fn(
            int $errorNumber,
            string $errorMessage,
            string $file,
            string $line,
            array $context
        ) => $logger->error(
            sprintf('%s. File: %s. Line: %s', $errorMessage, $file, $line)
        )
    );

    $imageServer = new ImageServer\Server(getenv('IMAGES_DIRECTORY'));
    $imageServer->handleRequest();
} catch (\Throwable $throwable) {
    $logger->error(sprintf('%s. File: %s. Line: %s. Stacktrace: %s', $throwable->getMessage(), $throwable->getFile(), $throwable->getLine(), $throwable->getTraceAsString()));
    if (getenv('ENVIRONMENT') === 'dev') {
        echo $throwable->getMessage();
    }
}
