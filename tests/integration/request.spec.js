describe('Test case', () => {
    before(() => {
        cy.task('renameFile', {source: '/var/www/html/.env', destination: '/var/www/html/.env.dev'});
        cy.task('renameFile', {source: '/var/www/html/.env.test', destination: '/var/www/html/.env'});
    }),
    context('Request validation', () => {
        it('Correct photo id should returns photo', () => {
            cy.request('/?pid=b6x0z').then(response => {
                expect(response.status).to.eq(200)
                expect(response.headers['content-type']).to.eq('image/png')
            })
        })
    }),
    after(() => {
        cy.task('renameFile', {source: '/var/www/html/.env', destination: '/var/www/html/.env.test'});
        cy.task('renameFile', {source: '/var/www/html/.env.dev', destination: '/var/www/html/.env'});
    })
})