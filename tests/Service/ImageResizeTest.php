<?php

declare(strict_types=1);

namespace Tests\Service;

use ImageServer\Model\Image;
use ImageServer\Service\ImageResize;
use PHPUnit\Framework\TestCase;

/**
 * Class ImageResizeTest
 * @package Tests\Service
 */
class ImageResizeTest extends TestCase
{
    public function testCalculateCorrectSizesReturnsTheSameSizes(): void
    {
        $source = (new Image())->setWidth(1024)->setHeight(768);
        $maximum = (new Image())->setWidth(2048)->setHeight(768);

        $result = ImageResize::calculateSize($source, $maximum);

        $this->assertEquals([1024, 768], [$result->getWidth(), $result->getHeight()]);
    }

    public function testCalculateCorrectSizesReturnsIncreasedSizes(): void
    {
        $source = (new Image())->setWidth(1024)->setHeight(768);
        $maximum = (new Image())->setWidth(2048)->setHeight(1024);

        $result = ImageResize::calculateSize($source, $maximum);

        $this->assertEquals([1365, 1024], [$result->getWidth(), $result->getHeight()]);
    }

    public function testCalculateCorrectSizesReturnsDecreasedSizes(): void
    {
        $source = (new Image())->setWidth(2048)->setHeight(1024);
        $maximum = (new Image())->setWidth(1024)->setHeight(768);

        $result = ImageResize::calculateSize($source, $maximum);

        $this->assertEquals([1536, 768], [$result->getWidth(), $result->getHeight()]);
    }
}
